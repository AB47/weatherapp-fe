import { KotrakWeatherAppPage } from './app.po';

describe('kotrak-weather-app App', function() {
  let page: KotrakWeatherAppPage;

  beforeEach(() => {
    page = new KotrakWeatherAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
